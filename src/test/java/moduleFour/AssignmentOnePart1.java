package moduleFour;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.BaseTest;

public class AssignmentOnePart1 extends BaseTest {
	private ChromeDriver driver;

	@BeforeTest
	public void beforeTestSetup() {
		driver = new ChromeDriver();
	}
	
	@AfterTest
	public void afterTestSetup() {
		driver.quit();
	}
	

	// - Navigate to bestbuy.ca
	// - Search for “10583501”
    // - Assert that the title of the resulting page “Samsung 88" 4K UHD HDR QLED Tizen Smart TV (QN88Q9FAMFXZC) - Charcoal Black”
    // - Assert that the title of the results product is “$29,999.99”
	@Test
	public void searchTestOne() throws Exception {
		
		// Navigate to bestbuy.ca
		driver.get("https://www.bestbuy.ca");
		
		// Add some code here to accommodate for the popup
		WebElement popup = null;
		try {
			popup = driver.findElement(By.className("at-newsletter-modal-container"));
		} catch (Exception ex) {
			// Do nothing
		}
		if (popup != null && popup.isDisplayed()) {
			WebElement closeButton = popup.findElement(By.className("at-close-icon"));
			closeButton.click();
			Thread.sleep(2000);
		}
		
		// Find the search bar
		WebElement searchBar = driver.findElement(By.id("ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_TxtSearchKeyword"));

		// type iphone
		searchBar.sendKeys("10583501");
		
		// Find the search icon
		WebElement searchIcon = driver.findElement(By.id("ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_BtnSubmitSearch"));
		
		// Click on the search icon
		searchIcon.click();
		
		// Find the heading element
		WebElement title = driver.findElement(By.className("product-title"));
		
		// Get the text
		String titleText = title.getText();
		
		Assert.assertEquals(titleText, "Samsung 88\" 4K UHD HDR QLED Tizen Smart TV (QN88Q9FAMFXZC) - Charcoal Black");

	}
	
	
	// - Navigate to bestbuy.ca
	// - Search for “10583535”
    // - Assert that the title of the resulting page “Samsung 75" 4K UHD HDR QLED Tizen Smart TV (QN75Q9FAMFXZC) - Charcoal Black”
    // - Assert that the title of the results product is “$9,999.99”
	@Test
	public void searchTestTwo() throws Exception {
		
		// Navigate to bestbuy.ca
		driver.get("https://www.bestbuy.ca");
		
		// Add some code here to accommodate for the popup
		WebElement popup = null;
		try {
			popup = driver.findElement(By.className("at-newsletter-modal-container"));
		} catch (Exception ex) {
			// Do nothing
		}
		if (popup != null && popup.isDisplayed()) {
			WebElement closeButton = popup.findElement(By.className("at-close-icon"));
			closeButton.click();
			Thread.sleep(2000);
		}
		
		// Find the search bar
		WebElement searchBar = driver.findElement(By.id("ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_TxtSearchKeyword"));

		// type iphone
		searchBar.sendKeys("10583535");
		
		// Find the search icon
		WebElement searchIcon = driver.findElement(By.id("ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_BtnSubmitSearch"));
		
		// Click on the search icon
		searchIcon.click();
		
		// Find the heading element
		WebElement title = driver.findElement(By.className("product-title"));
		
		// Get the text
		String titleText = title.getText();
		
		Assert.assertEquals(titleText, "Samsung 75\" 4K UHD HDR QLED Tizen Smart TV (QN75Q9FAMFXZC) - Charcoal Black"); 
		
	}
}
