package moduleFour;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.BaseTest;

public class AssignmentOnePart2 extends BaseTest {
	private ChromeDriver driver;

	@BeforeTest
	public void beforeTestSetup() {
		driver = new ChromeDriver();
	}
	
	@AfterTest
	public void afterTestSetup() {
		driver.quit();
	}
	

	// - Navigate to bestbuy.ca
    // - Navigate to the SHOP > TV & Home Theatre > 4K Ultra HD TVs
	// 	
    // - Assert that the title of the results page is “4K Ultra HD TVs”
	@Test
	public void navigationTestOne() throws Exception {
		
		// Navigate to bestbuy.ca
		driver.get("https://www.bestbuy.ca");
		
		// Add some code here to accommodate for the popup
		WebElement popup = null;
		try {
			popup = driver.findElement(By.className("at-newsletter-modal-container"));
		} catch (Exception ex) {
			// Do nothing
		}
		if (popup != null && popup.isDisplayed()) {
			WebElement closeButton = popup.findElement(By.className("at-close-icon"));
			closeButton.click();
			Thread.sleep(2000);
		}
		
		WebElement shopButton = driver.findElement(By.id("shop-menu-link"));
		shopButton.click();
		
		WebElement tvButton = driver.findElement(By.xpath("//*[@id=\"global-header\"]/nav[3]/div[2]/div/div[1]/ul/li[5]/a"));
		tvButton.click();
		
		WebElement fourKLink = driver.findElement(By.xpath("//*[@id=\"global-header\"]/nav[3]/div[2]/div/div[1]/ul/li[5]/div/div[1]/div/div/div[2]/ul[1]/li[1]/a"));
		fourKLink.click();
		
		WebElement heading = driver.findElement(By.id("ctl00_CC_SearchHeading"));
		String headingText = heading.getText();
		
		Assert.assertEquals(headingText, "4K Ultra HD TVs");
		
		
	}
}
