package moduleFour;

public class TestOne {

	
	public static void main(String[] args) {
		
		String name = "Azhar Iqbal";
		System.out.println(name);
		
		if (name.equals("Jose")) {
			System.out.println("My name is Jose");
		} else {
			System.out.println("My name is not Jose");
		}

		// I want to print each integer from 1 to 10
//		System.out.println("1");
//		System.out.println("2");
//		System.out.println("3");
//		System.out.println("4");
//		System.out.println("5");
//		System.out.println("6");
//		System.out.println("7");
//		System.out.println("8");
//		System.out.println("9");
//		System.out.println("10");
		
		for (int i = 1; i <= 10; i++) {
			System.out.println(i);
		}
		// i++ = (i = i + 1)
//		first iteration: 	i = 1 , i <= 10 = True; Print 1 , i++ = 2
// 		second iteration: 	i = 2 , i <= 10 = True; print 2 , i++ = 3
//		third iteration: 	i = 3 , i <= 10 = True; print 3 , i++ = 4
//		....
//		zasd iteration: 		i = 9, i <= 10 = True, print 9, i++ = 10
// 		alsk iteration		i = 10, i <= 10 = True, print 10, i++ = 11
// 		ladkj teration:		i = 11, i <= 10 = False ==> Break out of loop
		
		for (int x = 10; x >= 0; x-- ) {
			System.out.println(x);
		}
		
		
		
		
		
		
		

	}
}
