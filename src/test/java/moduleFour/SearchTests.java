package moduleFour;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.BaseTest;

public class SearchTests extends BaseTest {

	private ChromeDriver driver;

	@BeforeTest
	public void beforeTestSetup() {
		driver = new ChromeDriver();
	}
	
	@AfterTest
	public void afterTestSetup() {
		driver.quit();
	}
	
	@Test
	public void searchTestOne() {

		// Navigate to the bestbuy web page
		driver.get("http://www.bestbuy.ca");
		
		// Find the search bar
		WebElement searchBar = driver.findElement(By.id("ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_TxtSearchKeyword"));
		
		// Type something into the search bar
		searchBar.sendKeys("iphone");
		
		// Find the search icon
		WebElement searchIcon = driver.findElement(By.id("ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_BtnSubmitSearch")); 
		
		// Click on the search icon
		searchIcon.click();
		
		// Wait for the search results - Use an explicit wait and
		// wait for the results on the page to be present
		WebDriverWait wdw = new WebDriverWait(driver, 10);
		wdw.until(ExpectedConditions.presenceOfElementLocated(By.id("ctl00_CC_ProductSearchResultListing_SearchProductListing")));
		
		// Get the title of the page
		String pageTitle = driver.findElement(By.id("ctl00_CC_SearchHeading")).getText();
		
		// Assert that the title contains the search term
		Assert.assertEquals("Results for iPhone", pageTitle);
		
		
	}
}
