package moduleFour;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import common.BaseTest;

public class TestTwo extends BaseTest {

	// Todo:
	// 1: Navigate to bestbuy.ca
	// 2: Search for iphone
	// 		-- Find the search bar
	// 		-- Type iphone
	// 		-- Find the search icon
	// 		-- CZlick the search icon
	// 3: When we land on the results page, we are going to assert that the
	// 	  title equals "Results for iphone"
	
	@Test
	public void searchForIPhone() throws Exception {
		
		// Open up Chrome
		ChromeDriver driver = new ChromeDriver();
		
		// Navigate to bestbuy.ca
		driver.get("https://www.bestbuy.ca");
		
		// Add some code here to accommodate for the popup
		WebElement popup = null;
		try {
			popup = driver.findElement(By.className("at-newsletter-modal-container"));
		} catch (Exception ex) {
			// Do nothing
		}
		if (popup != null && popup.isDisplayed()) {
			WebElement closeButton = popup.findElement(By.className("at-close-icon"));
			closeButton.click();
			Thread.sleep(2000);
		}
		
		// Find the search bar
		WebElement searchBar = driver.findElement(By.id("ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_TxtSearchKeyword"));

		// type iphone
		searchBar.sendKeys("iphone");
		
		// Find the search icon
		WebElement searchIcon = driver.findElement(By.id("ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_BtnSubmitSearch"));
		
		// Click on the search icon
		searchIcon.click();
		
		// Find the heading element
		WebElement heading = driver.findElement(By.id("ctl00_CC_SearchHeading"));
		
		// Get the text
		String headingText = heading.getText();
		
		Assert.assertEquals("Results for iphone", headingText);
		
	}
	
	
	
	
	
}
