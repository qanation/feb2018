package moduleFive;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.BaseTest;
import common.NavigationItems;
import pageobjects.HomePage;
import pageobjects.SearchResultsPage;

public class Module4_AssignmentOnePart2 extends BaseTest {
	private ChromeDriver driver;

	@BeforeTest
	public void beforeTestSetup() {
		driver = new ChromeDriver();
	}
	
	@AfterTest
	public void afterTestSetup() {
		driver.quit();
	}
	

	// - Navigate to bestbuy.ca
    // - Navigate to the SHOP > TV & Home Theatre > 4K Ultra HD TVs
    // - Assert that the title of the results page is “4K Ultra HD TVs”
	@Test
	public void navigationTestOne() throws Exception {
		
		// Navigate to bestbuy.ca
		driver.get("https://www.bestbuy.ca");
	}
}
