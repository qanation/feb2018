package moduleFive;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import common.BaseTest;
import pageobjects.*;

public class Module4_AssignmentOnePart1 extends BaseTest {
	private ChromeDriver driver;

	@BeforeTest
	public void beforeTestSetup() {
		driver = new ChromeDriver();
	}
	
	@AfterTest
	public void afterTestSetup() {
		driver.quit();
	}
	

	// - Navigate to bestbuy.ca
	// - Search for “10583501”
    // - Assert that the title of the resulting page “Samsung 88" 4K UHD HDR QLED Tizen Smart TV (QN88Q9FAMFXZC) - Charcoal Black”
    // - Assert that the title of the results product is “$29,999.99”
	@Test
	public void searchTestOne() throws Exception {
		
		// Navigate to bestbuy.ca
		driver.get("https://www.bestbuy.ca");
		
		HomePage hp = new HomePage(driver);
		ProductPage pp = hp.searchByWebcode("10583501");
		Assert.assertEquals(pp.getProductTitle(), "Samsung 88\" 4K UHD HDR QLED Tizen Smart TV (QN88Q9FAMFXZC) - Charcoal Black");
		
	}
	
	
	// - Navigate to bestbuy.ca
	// - Search for “10583535”
    // - Assert that the title of the resulting page “Samsung 75" 4K UHD HDR QLED Tizen Smart TV (QN75Q9FAMFXZC) - Charcoal Black”
    // - Assert that the title of the results product is “$9,999.99”
	@Test
	public void searchTestTwo() throws Exception {
		
		// Navigate to bestbuy.ca
		driver.get("https://www.bestbuy.ca");
		
	}
}
