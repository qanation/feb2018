package moduleSix;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Scanner;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;

import common.BaseTest;
import pageobjects.*;

public class Module6_SearchTests_ReadFromFile extends BaseTest {
	private ChromeDriver driver;

	@BeforeTest
	public void beforeTestSetup() {
		driver = new ChromeDriver();
	}
	
	@AfterTest
	public void afterTestSetup() {
		driver.quit();
	}

	@DataProvider(name = "search-test-data-file", parallel = false)
	public Object[][] searchDataProvider(Method testMethod, ITestContext context) throws Exception {
		
		String filePath = "./src/test/resources/testdata/searchdata.csv";
		Reader reader = new FileReader(new File(filePath));
		CSVReader csvReader = new CSVReader(reader);
		List<String[]> records = csvReader.readAll();
		Object[][] data = new Object[records.size()][4];

		int i = 0;
		for (String[] record : records) {
			data[i] = record;
			i++;
		}
		csvReader.close();
		return data;
	}

	// - Navigate to bestbuy.ca
	// - Search for a web code
	// - Assert the web code
	// - Assert the model number
    // - Assert the title
    // - Assert the price
	@Test(dataProvider="search-test-data-file")
	public void searchTest_DataProvider_FromFile(String webCode, String modelNumber, String pageTitle, String price) throws Exception {
		
		// Navigate to bestbuy.ca
		driver.get("https://www.bestbuy.ca");
		
		HomePage hp = new HomePage(driver);
		ProductPage pp = hp.searchByWebcode(webCode);
		Assert.assertEquals(pp.getWebCode(), webCode);
		Assert.assertEquals(pp.getModelNumber(), modelNumber);
		Assert.assertEquals(pp.getProductTitle(), pageTitle);
		Assert.assertEquals(pp.getProductPrice(), price);
		
	}
	
	public static void main(String[] args) throws Exception {
		

		
		String as = "";

		
	}
}
