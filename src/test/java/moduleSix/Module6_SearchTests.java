package moduleSix;

import java.lang.reflect.Method;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import common.BaseTest;
import pageobjects.*;

public class Module6_SearchTests extends BaseTest {
	private ChromeDriver driver;

	@BeforeTest
	public void beforeTestSetup() {
		driver = new ChromeDriver();
	}
	
	@AfterTest
	public void afterTestSetup() {
		driver.quit();
	}

	@DataProvider(name = "search-test-data", parallel = false)
	public Object[][] searchDataProvider(Method testMethod, ITestContext context) throws Exception {
		return new Object[][] {
				// Webcode, Model Number, Page Title, Price
				{ "10583501", "QN88Q9FAMFXZC",
						"Samsung 88\" 4K UHD HDR QLED Tizen Smart TV (QN88Q9FAMFXZC) - Charcoal Black", "30,045.99" },
				{ "10583535", "QN75Q9FAMFXZC",
						"Samsung 75\" 4K UHD HDR QLED Tizen Smart TV (QN75Q9FAMFXZC) - Charcoal Black", "$10,045.99" },
				{ "10583534", "QN65Q9FAMFXZC",
						"Samsung 65\" 4K UHD HDR QLED Tizen Smart TV (QN65Q9FAMFXZC) - Charcoal Black", "$5,545.99" } };
	}

	// - Navigate to bestbuy.ca
	// - Search for a web code
	// - Assert the web code
	// - Assert the model number
    // - Assert the title
    // - Assert the price
	@Test(dataProvider="search-test-data")
	public void searchTest_DataProvider(String webCode, String modelNumber, String pageTitle, String price) throws Exception {
		
		// Navigate to bestbuy.ca
		driver.get("https://www.bestbuy.ca");
		
		HomePage hp = new HomePage(driver);
		ProductPage pp = hp.searchByWebcode(webCode);
		Assert.assertEquals(pp.getWebCode(), webCode);
		Assert.assertEquals(pp.getModelNumber(), modelNumber);
		Assert.assertEquals(pp.getProductTitle(), pageTitle);
		Assert.assertEquals(pp.getProductPrice(), price);
		
	}
}
