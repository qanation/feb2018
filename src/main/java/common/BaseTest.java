package common;

import org.testng.annotations.BeforeSuite;

public class BaseTest {

	private String privateString;
	public String publicString;
	
	@BeforeSuite
	public void beforeSuiteSetup() {
		String operatingSystem = System.getProperty("os.name");
		String projectFolder = System.getProperty("user.dir");
		if (operatingSystem.contains("Windows")) {
			System.setProperty("webdriver.chrome.driver", projectFolder + "/src/test/resources/webdrivers/chromedriver.exe");
		} else {
			System.setProperty("webdriver.chrome.driver",  projectFolder + "/src/test/resources/webdrivers/chromedriver");
		}
	}
}
