package common;

public enum NavigationItems {

	// Shop Menu Links
	Shop_TvAndHomeTheatre_4KUltraHDTVs("Shop > TV & Home Theatre > 4k Ultra HD TVs",
			new String[] { "id:shop-menu-link", "xpath: //*[@id=\"global-header\"]/nav[3]/div[2]/div/div[1]/ul/li[5]", "xpath://*[@id=\"global-header\"]/nav[3]/div[2]/div/div[1]/ul/li[5]/div/div[1]/div/div/div[2]/ul[1]/li[1]" });
	
	private final String title;
	private final String[] path;
	

	private NavigationItems(String title, String[] path) {
		this.title = title;
		this.path = path;
	}

	public String[] getPath() {
		return path;
	}
	
	public String getTitle() {
		return title;
	}
}
