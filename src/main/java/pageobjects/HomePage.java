package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage {

	public HomePage(WebDriver driver) throws Exception {
		this.driver = driver;
		// Initialize all the FindBy elements. NOTE: THIS IS IMPORTANT
		PageFactory.initElements(driver, this);
		this.waitForPageToLoad();
		this.handlePopup();
	}
	
	private void handlePopup() throws InterruptedException {
		WebElement popup = null;
		try {
			popup = driver.findElement(By.className("at-newsletter-modal-container"));
		} catch (Exception ex) {
			// Do nothing
		}
		if (popup != null && popup.isDisplayed()) {
			WebElement closeButton = popup.findElement(By.className("at-close-icon"));
			closeButton.click();
			Thread.sleep(2000);
		}
	}
}
