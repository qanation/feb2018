package pageobjects;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.NavigationItems;

public class BasePage {

	WebDriver driver;

	// Navigation Elements
	@FindBy(id = "shop-menu-link")
	protected WebElement shopMenuLink;
	
	@FindBy(id = "brands-menu-link")
	protected WebElement brandsMenuLink;
	
	@FindBy(id = "ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_TxtSearchKeyword")
	protected WebElement searchBar;
	
	@FindBy(id = "ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_BtnSubmitSearch")
	protected WebElement searchButton;
	
	@FindBy(id = "ctl00_MasterHeader_ctl00_uchead_ucWelcome_linkSignIn")
	protected WebElement signInLink;
	
	public SigninPage clickSigninLink() {
		this.signInLink.click();
		return new SigninPage(this.driver);
	}
	
	public void waitForPageToLoad() {
		WebDriverWait wdw = new WebDriverWait(driver, 30);
		wdw.pollingEvery(1, TimeUnit.SECONDS);
		wdw.until(ExpectedConditions.presenceOfElementLocated(By.id("ctl00_MasterHeader_ctl00_uchead_GlobalSearchUC_TxtSearchKeyword")));
	}

	public ProductPage searchByWebcode(String term) {
		this.searchBar.clear();
		this.searchBar.sendKeys(term);
		this.searchButton.click();
		return new ProductPage(this.driver);
	}

	public SearchResultsPage searchByTerm(String term) {
		this.searchBar.clear();
		this.searchBar.sendKeys(term);
		this.searchButton.click();
		return new SearchResultsPage(this.driver);
	}

	public SearchResultsPage navigateToPageViaMenu(NavigationItems nav) throws Exception {
		String[] path = nav.getPath();
		
		for (int i = 0; i < path.length; i++) {
			String p = path[i];
			WebElement el = null;
			if (p.startsWith("class:")) {
				el = this.driver.findElement(By.className(p.replaceAll("class:", "")));
			} else if (p.startsWith("id:")) {
				el = this.driver.findElement(By.id(p.replaceAll("id:", "")));
			} else if (p.startsWith("xpath:")) {
				el = this.driver.findElement(By.xpath(p.replaceAll("xpath:", "")));
			}
			
			if (el == null) {
				throw new Exception("Unsupported Navigation path: " + nav.getTitle() + " : Please review the IDs in NavigationItems enum");
			}
			
			el.click();
			Thread.sleep(500);
		}

		return new SearchResultsPage(this.driver);

	}
}
