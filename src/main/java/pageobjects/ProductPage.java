package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProductPage extends BasePage {
	
	@FindBy(id = "ctl00_CP_ctl00_PD_lblModelNumber")
	protected WebElement modelNumber;
	
	@FindBy(id = "ctl00_CP_ctl00_PD_lblSku")
	protected WebElement webCode;
	
	@FindBy(className = "product-title")
	protected WebElement productTitle;
	
	@FindBy(xpath="//*[@id=\"schemaorg-offer\"]/div[1]/div[1]/div[1]/span")
	protected WebElement productPrice;
	
	public ProductPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.waitForPageToLoad();
	}

	public String getModelNumber() {
		return this.modelNumber.getText();
	}
	
	public String getWebCode() {
		return this.webCode.getText();
	}

	public String getProductTitle() {
		return this.productTitle.getText();
	}

	public String getProductPrice() {
		return this.productPrice.getText();
	}
}
