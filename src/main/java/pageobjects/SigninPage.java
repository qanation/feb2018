package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SigninPage extends BasePage {

	@FindBy(id = "ctl00_CP_SignInUC1_UserNameContainer_txtUserName")
	protected WebElement emailAddress;
	
	@FindBy(id = "ctl00_CP_SignInUC1_PasswordContainer_txtPassword")
	protected WebElement password;
	
	@FindBy(id = "ctl00_CP_SignInUC1_BtnLoginButton")
	protected WebElement signinButton;
	
	public SigninPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.waitForPageToLoad();
	}
	
	public HomePage signIn(String username, String password) throws Exception {
		this.emailAddress.clear();
		this.emailAddress.sendKeys(username);
		this.password.clear();
		this.password.sendKeys(password);
		this.signinButton.click();
		return new HomePage(this.driver);
	}
	
	
	
	
	// Create page object with name: CreateAccountPage
	// 		-- Track all the elements you are going to interact with (i.e. first name, last name, etc.
	//		-- Add a method named fillForm(...) <--- pass in all the information as parameters
	//		-- 
	// Create a method in SigninPage with name: clickCreateAccount()
	// 		-- This method takes you to the CreateAccountPage
	// 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
