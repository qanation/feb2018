package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchResultsPage extends BasePage {

	//ctl00_CC_SearchHeading
	@FindBy(id = "ctl00_CC_SearchHeading")
	protected WebElement searchHeading;
	
	public SearchResultsPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		this.waitForPageToLoad();
	}

	public String getSearchHeading() {
		return this.searchHeading.getText();
	}
}
